﻿using HtmlAgilityPack;
using ReplaceWithHTMLNumbers.Core;
using ReplaceWithHTMLNumbers.Objects;
using System;
using System.Collections.Generic;
using System.Text;

namespace ReplaceWithHTMLNumbers.Interfaces
{
    public interface IHTMLService
    {
        bool OverrideHTML(string path, string repairedPath);
        string RepairHTML(string path);
        List<Symbol> CheckHTMLCharacters(string filePath);
        List<string> CheckHTMLSyntax(string path);
        Report GetHTMLReport(string path);
        HtmlDocument LoadHTML(string path, LoadFromEnum loadFromEnum = LoadFromEnum.File, bool fixEDM = false);
        void ReplaceHTMLTrackingCodes(string Path, string Code, bool TestLinks);
        void TestHTMLLinks(string path);
        void TestHTMLImages(string path);
        void SaveToRepo(string path);
        List<HTMLNodeExtended> CheckHTMLExternalLinks(string path, string search);

    }
}
