﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReplaceWithHTMLNumbers.Interfaces
{
    public interface IProcessService
    {
      void RunProcess(string path, ApplicationTypeEnum applicationTypeEnum = ApplicationTypeEnum.Chrome);
    }
}
