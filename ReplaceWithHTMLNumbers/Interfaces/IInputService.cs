﻿namespace ReplaceWithHTMLNumbers.Services
{
    public interface IInputService
    {
        void CLearWrite(string text);
        void ClearWriteTitle(string text);
        string GetInput(string text);
        bool GetInputBool(string text);
        bool GetBool(string input);
    }
}