﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReplaceWithHTMLNumbers.Interfaces
{
    public interface ISymbolService
    {
         List<Symbol> GetSymbols();
    }
}
