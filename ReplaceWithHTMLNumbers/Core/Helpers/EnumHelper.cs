﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReplaceWithHTMLNumbers.Core.Helpers
{
    public class EnumHelper
    {
        public static string GetApplicationType(ApplicationTypeEnum application)
        {
            //CAREFULL WHEN CHANGING PROGRAM FILES X86 IT HAS A SPACE!!
            string app = "";

            switch (application)
            {
                case ApplicationTypeEnum.Chrome:
                    app = @"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe";
                    break;
                case ApplicationTypeEnum.Edge:
                    app = @"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe";
                    break;
                case ApplicationTypeEnum.FireFox:
                    app = @"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe";
                    break;
                case ApplicationTypeEnum.IE:
                    app = @"C:\Program Files (x86)\Internet Explorer\iexplore.exe";
                    break;
            }

            return app;
        }
    }
}
