﻿using HtmlAgilityPack;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using ReplaceWithHTMLNumbers.Core;
using ReplaceWithHTMLNumbers.Interfaces;
using ReplaceWithHTMLNumbers.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml.Linq;

namespace ReplaceWithHTMLNumbers
{


    class Program
    {
        static void Main(string[] args)
        {
            //Add Services
            var serviceProvider = new ServiceCollection()
                .AddSingleton<IMenuService, MenuService>()
                .AddSingleton<IPDFGeneratonService, PDFGenerationService>()
                .AddSingleton<IProcessService, ProcessService>()
                .AddSingleton<IHTMLService, HTMLService>()
                .AddSingleton<ISymbolService, SymbolService>()
                .AddSingleton<IURLService, URLService>()
                .AddSingleton<IInputService, InputService>()
                .BuildServiceProvider();
            var _PDFGenerationService = serviceProvider.GetService<IPDFGeneratonService>();
            var _ProcessService = serviceProvider.GetService<IProcessService>();
            var _HTMLService = serviceProvider.GetService<IHTMLService>();
            var _SymbolService = serviceProvider.GetService<ISymbolService>();
            var _URLService = serviceProvider.GetService<IURLService>();
            var _MenuService = serviceProvider.GetService<IMenuService>();
            var _InputService = serviceProvider.GetService<IInputService>();


            //Ask user for path to EDM.
            var path = _InputService.GetInput("Enter Path to File...");


            while (true)
            {
                Console.Clear();
                var menuChoice = _MenuService.MenuSelection(path);
                if(menuChoice == "7")
                {
                    break;
                }
            }

        }
    }
}
