﻿using HtmlAgilityPack;
using ReplaceWithHTMLNumbers.Objects;
using System;
using System.Collections.Generic;
using System.Text;

namespace ReplaceWithHTMLNumbers
{
    public class Report
    {
        public List<Symbol> Symbols { get; set; }
        public List<string> SyntaxIssues { get; set; }
        public List<HTMLNodeExtended> Hyperlinks { get; set; }
        public List<HTMLNodeExtended> Images { get; set; }

    }
}
