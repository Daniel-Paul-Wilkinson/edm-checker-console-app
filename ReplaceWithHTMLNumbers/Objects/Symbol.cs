﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReplaceWithHTMLNumbers
{
    public class Symbol
    {
        public string ID { get; set; }
        public string SpecialCharacter { get; set; }
        public string HTMLEntityNumber { get; set; }
        public string HTMLEntityName { get; set; }
        public string HtmlEntityDescription { get; set; }
        public int Occurrences { get; set; }
    }
}
