﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Text;

namespace ReplaceWithHTMLNumbers.Objects
{
    public class HTMLNodeExtended
    {
        public HtmlNode Node { get; set; }
        public bool Valid { get; set; }
    }
}
