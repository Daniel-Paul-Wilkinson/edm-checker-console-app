﻿using Newtonsoft.Json;
using ReplaceWithHTMLNumbers.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ReplaceWithHTMLNumbers.Services
{
    public class SymbolService : ISymbolService
    {
        public List<Symbol> GetSymbols()
        {
            return JsonConvert.DeserializeObject<List<Symbol>>(File.ReadAllText("Content/symbols.json"));
        }
    }
}
