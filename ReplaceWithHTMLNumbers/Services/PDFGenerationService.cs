﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using ReplaceWithHTMLNumbers.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace ReplaceWithHTMLNumbers
{
    public class PDFGenerationService : IPDFGeneratonService
    {
        private readonly IProcessService _processService;
        public PDFGenerationService(IProcessService processService)
        {
            _processService = processService;
        }

        public void GenerateReport(Report report)
        {
            //Create PDF directory if it does not exist
            CreateDiretory(@"Report");

            var guid = Guid.NewGuid().ToString();

            //Create report file stream to save to.
            FileStream fs = new FileStream(@"Report" + "/" + DateTime.Now.ToString("yyyyMMddTHHmmss") + ".pdf", FileMode.Create, FileAccess.ReadWrite, FileShare.Read);

            //Setup page size and margins.
            Document pdf = new Document(PageSize.A4, 20, 20, 30, 30);

            //Create new pdf instance.
            PdfWriter writer = PdfWriter.GetInstance(pdf, fs);

            //Add Meta Data to pdf
            pdf = GetPDFMetaData(pdf);

            //Open pdf to write content.
            pdf.Open();

            //Add a title of the document
            pdf.Add(new Paragraph($"EDM Report - { DateTime.Now.ToShortTimeString()}", FontFactory.GetFont("Arial", 30, BaseColor.BLACK)));

            //Add Characters which need to be changed.
            if (report.Symbols.Any(x => x.Occurrences > 0))
            {
                pdf.Add(new Paragraph($"Special Characters", FontFactory.GetFont("Arial", 25, BaseColor.BLACK)));
                List list = new List(List.UNORDERED);
                list.SetListSymbol("\u2022");
                foreach (var symbol in report.Symbols.Where(x => x.Occurrences > 0).OrderByDescending(x => x.Occurrences))
                {
                    list.Add(new ListItem(new Phrase($" Found {symbol.Occurrences} Occurrences of '{symbol.SpecialCharacter}' replace this with '{GetEntityName(symbol)}'", FontFactory.GetFont("Arial", 11, BaseColor.BLACK))));
                }
                pdf.Add(list);
            }

            //Add Syntax errors to pdf
            if (report.SyntaxIssues.Count > 0)
            {
                pdf.Add(new Paragraph($"HTML Errors", FontFactory.GetFont("Arial", 25, BaseColor.BLACK)));
                List list = new List(List.UNORDERED);
                list.SetListSymbol("\u2022");
                foreach (var error in report.SyntaxIssues)
                {
                    list.Add(new ListItem(new Phrase($"{error}", FontFactory.GetFont("Arial", 11, BaseColor.BLACK))));
                }
                pdf.Add(list);
            }

            if (report.Hyperlinks.Count > 0)
            {
                pdf.Add(new Paragraph($"Hyperlinks", FontFactory.GetFont("Arial", 25, BaseColor.BLACK)));
                List list = new List(List.UNORDERED);
                list.SetListSymbol("\u2022");
                foreach (var node in report.Hyperlinks)
                {
                    list.Add(new ListItem(new Phrase($"{node.Node.GetAttributeValue("href", string.Empty) ?? ""} - {(node.Valid ? "This is a valid URL" : "this is not a Valid URL")}", FontFactory.GetFont("Arial", 11, BaseColor.BLACK))));
                }
                pdf.Add(list);
            }

            if (report.Images.Count > 0)
            {
                pdf.Add(new Paragraph($"Images", FontFactory.GetFont("Arial", 25, BaseColor.BLACK)));
                List list = new List(List.UNORDERED);
                list.SetListSymbol("\u2022");
                foreach (var node in report.Images)
                {
                    list.Add(new ListItem(new Phrase($"{node.Node.GetAttributeValue("src", string.Empty) ?? ""} - {(node.Valid ? "This is a valid URL" : "this is not a Valid URL")}", FontFactory.GetFont("Arial", 11, BaseColor.BLACK))));
                }
                pdf.Add(list);
            }

            //close all streams
            pdf.Close();

            writer.Close();

            fs.Close();


            //open report for user.
            _processService.RunProcess(fs.Name);

        }

        private void CreateDiretory(string directory)
        {
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
        }

        private Document GetPDFMetaData(Document pdf)
        {
            pdf.AddAuthor("Daniel Wilkinson");
            pdf.AddCreator("Daniel Wilkinson");
            pdf.AddKeywords("EDM Report");
            pdf.AddSubject("EDM Report");
            pdf.AddTitle("EDM Report");

            return pdf;
        }

        private string GetEntityName(Symbol symbol)
        {
            if (symbol.HTMLEntityName.Length > 0)
                return symbol.HTMLEntityName;
            else
                return symbol.HTMLEntityNumber;
        }
    }
}
