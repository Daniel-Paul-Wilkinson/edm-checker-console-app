﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReplaceWithHTMLNumbers.Services
{
    public class InputService : IInputService
    {
        public void ClearWriteTitle(string text)
        {
            Console.Clear();
            Console.WriteLine("-------------------------");
            Console.WriteLine(text);
            Console.WriteLine("-------------------------");
        }

        public string GetInput(string text)
        {
            Console.Clear();
            Console.WriteLine(text);
            return Console.ReadLine();
        }
        public bool GetInputBool(string text)
        {
            Console.Clear();
            Console.WriteLine(text);
            return GetBool(Console.ReadLine());
        }

        public bool GetBool(string input)
        {
            return (input.ToUpper() == "Y");
        }

        public void CLearWrite(string text)
        {
            Console.Clear();
            Console.WriteLine(text);
        }
    }
}
