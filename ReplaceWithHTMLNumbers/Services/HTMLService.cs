﻿using HtmlAgilityPack;
using ReplaceWithHTMLNumbers.Core;
using ReplaceWithHTMLNumbers.Interfaces;
using ReplaceWithHTMLNumbers.Objects;
using ReplaceWithHTMLNumbers.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;

namespace ReplaceWithHTMLNumbers
{
    public class HTMLService : IHTMLService
    {

        private readonly IProcessService _processService;
        private readonly ISymbolService _symbolService;
        private readonly IInputService _inputService;
        private readonly IURLService _URLService;


        public HTMLService(IProcessService processService, ISymbolService symbolService, IInputService inputService, IURLService uRLService)
        {
            _processService = processService;
            _symbolService = symbolService;
            _inputService = inputService;
            _URLService = uRLService;
        }

        public void ReplaceHTMLTrackingCodes(string Path, string Code, bool TestLinks)
        {
            var doc = LoadHTML(Path, LoadFromEnum.File);

            foreach (HtmlNode link in SearchHTMLForNodes(DocumentSearchConst.ALL_A_TAG, null, doc))
            {
                string href = link.GetAttributeValue("href", string.Empty);

                if (href == "")
                {
                    //TODO: Make note on which href have no links incase this is issue in HTML.
                    continue;
                }

                if (href.Contains("utm_source"))
                {
                    //www.example.com?utm_source=newsletter&utm_medium=Email&utm_campaign=189039"
                    var Token = new UriBuilder(Code);
                    var TokenQuery = HttpUtility.ParseQueryString(Token.Query);
                    var CurrentHref = new UriBuilder(href);
                    var CurrentHrefQuery = HttpUtility.ParseQueryString(CurrentHref.Query);
                    CurrentHrefQuery["utm_source"] = TokenQuery["utm_source"];
                    CurrentHrefQuery["utm_medium"] = TokenQuery["utm_medium"];
                    CurrentHrefQuery["utm_campaign"] = TokenQuery["utm_campaign"];
                    CurrentHref.Query = CurrentHrefQuery.ToString();
                    href = CurrentHref.ToString();
                }
                if (TestLinks)
                {

                    _processService.RunProcess(href);

                    var confirm = _inputService.GetInputBool($"Does this link look ok ({href})?");

                    if (confirm)
                    {
                        link.SetAttributeValue("href", href);
                    }
                }
                else
                {
                    link.SetAttributeValue("href", href);
                }

            }
            doc.Save(Path);
        }

        public List<HtmlNode> SearchHTMLForNodes(string searchterm, string path = "", HtmlDocument doc = null)
        {
            if (doc == null)
            {
                doc = LoadHTML(path, LoadFromEnum.File);
            }

            List<HtmlNode> elements = new List<HtmlNode>();

            foreach (HtmlNode element in doc.DocumentNode.SelectNodes(searchterm))
            {
                elements.Add(element);
            }

            return elements;
        }

        public HtmlDocument LoadHTML(string path, LoadFromEnum loadFromEnum = LoadFromEnum.File, bool fixEDM = false)
        {
            var document = new HtmlDocument();
            if (!fixEDM)
            {
                document.OptionFixNestedTags = false;
            }

            if (loadFromEnum == LoadFromEnum.File)
            {
                document.Load(path);
            }
            else if (loadFromEnum == LoadFromEnum.Web)
            {
                try
                {
                    HtmlWeb web = new HtmlWeb();
                    document = web.Load(path);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Please check internet connection...");
                }
            }

            return document;
        }

        public bool OverrideHTML(string path, string repairedPath)
        {
            try
            {
                //get the text from the repaired EDM.
                var repaired = File.ReadAllText(repairedPath);

                //override all the text in the path file.
                File.WriteAllText(path, repaired);

                //open the path file so the user can check this has happened correctly!
                _processService.RunProcess(path);

                return true;

            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public string RepairHTML(string path)
        {
            //load the document.
            var doc = LoadHTML(path, LoadFromEnum.File, true);

            //Add a new random string to the file when we save it.
            var guid = Guid.NewGuid();

            //Save the document.
            doc.Save($@"EDM/{guid}test.html");

            //Open the file that is repaired so the user can see if it looks correct!
            _processService.RunProcess($@"EDM/{guid}test.html");

            //return the repaired html path.
            return $@"EDM/{guid}test.html";

        }

        public Report GetHTMLReport(string path)
        {
            return new Report()
            {
                Symbols = CheckHTMLCharacters(path),
                SyntaxIssues = CheckHTMLSyntax(path),
                Hyperlinks = CheckHTMLExternalLinks(path,DocumentSearchConst.ALL_A_TAG),
                Images = CheckHTMLExternalLinks(path,DocumentSearchConst.ALL_IMG_TAG),
            };
        }

        public List<HTMLNodeExtended> CheckHTMLExternalLinks(string path, string search)
        {
            List<HTMLNodeExtended> Nodes = new List<HTMLNodeExtended>();

            var nodes = SearchHTMLForNodes(search, path);

            foreach (var node in nodes)
            {
                HTMLNodeExtended vu = new HTMLNodeExtended();
                vu.Node = node;

                var attribute = "";
                switch (vu.Node.Name)
                {
                    case "img":
                        attribute = "src";
                        break;
                    case "a":
                        attribute = "href";
                        break;
                }

                string href = node.GetAttributeValue(attribute, string.Empty);
                if (href != null && href.Length > 0)
                {
                    vu.Valid = _URLService.URlIsValid(href);
                }
                else
                {
                    vu.Valid = false;
                    continue;
                }
                Nodes.Add(vu);

            }

            return Nodes;

        }

        public List<string> CheckHTMLSyntax(string path)
        {
            var doc = LoadHTML(path, LoadFromEnum.File, false);

            List<string> errors = new List<string>();
            var parseErrors = doc.ParseErrors;

            if (parseErrors != null)
            {
                foreach (var htmlParseError in parseErrors)
                {
                    switch (htmlParseError.Code)
                    {
                        case HtmlParseErrorCode.TagNotClosed:
                            errors.Add(" Error: Tag(s) are not closed properly.");
                            break;
                        case HtmlParseErrorCode.CharsetMismatch:
                            errors.Add(" Error: Charset Mix Match.");
                            break;
                        case HtmlParseErrorCode.EndTagInvalidHere:
                            errors.Add(" Error: Incorrect end tag(s) placement.");
                            break;
                        case HtmlParseErrorCode.EndTagNotRequired:
                            errors.Add(" Error: End Tag(s) not required here.");
                            break;
                        case HtmlParseErrorCode.TagNotOpened:
                            errors.Add(" Error: Tag(s) not opened.");
                            break;
                    }
                }
            }

            return errors;
        }

        public List<Symbol> CheckHTMLCharacters(string filePath)
        {
            //read all lines
            var lines = File.ReadLines(filePath);

            //get all possible incorrect characters
            var symbols = _symbolService.GetSymbols();

            //3. count all of the occurences of the lefft over symbols
            foreach (var line in lines)
            {
                foreach (var symbol in symbols)
                {
                    if (line.Contains(symbol.SpecialCharacter))
                    {
                        foreach (char character in line)
                        {
                            if (symbol.SpecialCharacter != null && character.ToString() == symbol.SpecialCharacter)
                            {
                                symbol.Occurrences = symbol.Occurrences += 1;
                            }
                        }
                    }
                }
            }

            return symbols;
        }


        public void SaveToRepo(string path)
        {
            throw new NotImplementedException();
        }

        public void TestHTMLLinks(string path)
        {

        }

        public void TestHTMLImages(string path)
        {
        }
    }
}
