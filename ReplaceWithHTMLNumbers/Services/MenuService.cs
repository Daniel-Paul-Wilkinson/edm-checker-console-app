﻿using ReplaceWithHTMLNumbers.Interfaces;
using ReplaceWithHTMLNumbers.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace ReplaceWithHTMLNumbers
{
    public class MenuService : IMenuService
    {

        private readonly IProcessService _processService;
        private readonly ISymbolService _symbolService;
        private readonly IHTMLService _htmlService;
        private readonly IURLService _URLService;
        private readonly IPDFGeneratonService _PDFGeneratonService;
        private readonly IInputService _inputService;


        public MenuService(IPDFGeneratonService PDFGeneratonService,IProcessService processService, IInputService inputService,
            ISymbolService symbolService, IHTMLService htmlService, IURLService uRLService)
        {
            _processService = processService;
            _symbolService = symbolService;
            _htmlService = htmlService;
            _URLService = uRLService;
            _PDFGeneratonService = PDFGeneratonService;
            _inputService = inputService;
        }


        public void CLearWrite(string text)
        {
            Console.Clear();
            Console.WriteLine(text);
        }

       public string MenuSelection(string path)
        {
            int num;
            string userChoice;

            do
            {
                Console.WriteLine("Select the number of your choice: ");
                Console.WriteLine("1: Produce EDM Report & (Optionally) Preform HTML Syntactic Repair"); 
                Console.WriteLine("2: Test Images");
                Console.WriteLine("3: Test Links");
                Console.WriteLine("4: Apply UTM Codes");
                Console.WriteLine("5: Replace Symbols wirh HTML Entity Codes");
                Console.WriteLine("6: Save Changes and push to EDM Store");
                Console.WriteLine("7: Quit");
                Console.Write("Enter the number of your choice: ");
                userChoice = Console.ReadLine();

                if (!Int32.TryParse(userChoice, out num)) continue;

                switch (userChoice)
                {
                    case "1":
                        var Report = _htmlService.GetHTMLReport(path);
                        _PDFGeneratonService.GenerateReport(Report);
                        if (Report.SyntaxIssues.Count > 0)
                        {
                            var repairedPath = "";

                            var repair = _inputService.GetInputBool("Preform HTML Syntactic Repair? Y/N:");

                            if (repair)
                            {
                                repairedPath = _htmlService.RepairHTML(path);
                            }

                            var result = _inputService.GetInputBool("Would you like to override the previous virsion with the repaired? Y/N");

                            if (result)
                            {
                                _htmlService.OverrideHTML(path, repairedPath);
                            }
                        }
                        else
                        {
                            _inputService.CLearWrite("No syntactic issues, press any key to contine.");
                            Console.ReadKey();

                        }
                        break;
                    case "2":
                        var imgnodes = _htmlService.CheckHTMLExternalLinks(path, DocumentSearchConst.ALL_IMG_TAG);
                        foreach(var node in imgnodes)
                        {
                            _processService.RunProcess(node.Node.GetAttributeValue("src",string.Empty), ApplicationTypeEnum.Chrome);
                        }

                        break;
                    case "3":
                        var anodes = _htmlService.CheckHTMLExternalLinks(path, DocumentSearchConst.ALL_A_TAG);
                        foreach (var node in anodes)
                        {
                            _processService.RunProcess(node.Node.GetAttributeValue("href", string.Empty), ApplicationTypeEnum.Chrome);
                        }
                        break;
                    case "4":
                        var Token = _inputService.GetInput("Please paste your UTM code below." + Environment.NewLine +
                                             "You can generate one here at:(https://ga-dev-tools.appspot.com/campaign-url-builder/)");

                        var TestLinks = _inputService.GetInputBool("Would you like to open each link that will be effected in a browser for review?");

                        if (Token != null)
                        {
                            _htmlService.ReplaceHTMLTrackingCodes(path, Token, TestLinks);
                        }
                        break;
                    case "5":
                        _htmlService.SaveToRepo(path);
                        break;
                    case "6":
                        _htmlService.SaveToRepo(path);
                        break;
                    case "7":
                        Environment.Exit(0);
                        break;
                }

                return userChoice;

            } while (true);

        }
    }
}
