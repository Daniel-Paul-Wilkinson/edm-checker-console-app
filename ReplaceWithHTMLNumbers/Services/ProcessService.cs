﻿using ReplaceWithHTMLNumbers.Core.Helpers;
using ReplaceWithHTMLNumbers.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace ReplaceWithHTMLNumbers.Services
{
    public class ProcessService : IProcessService
    { 
        /// <summary>c
        /// Opens a file in chrome
        /// </summary>
        /// <param name="app">Application to open in</param>
        /// <param name="path">Path to a file to open</param>
        public void RunProcess(string path, ApplicationTypeEnum applicationTypeEnum = ApplicationTypeEnum.Chrome)
        {
            if (path != null && path.Length > 0) {
                ProcessStartInfo info = new ProcessStartInfo(EnumHelper.GetApplicationType(applicationTypeEnum));
                info.CreateNoWindow = true;
                info.UseShellExecute = false;
                info.Arguments = path;
                Process.Start(info);
            }
        }
    }
}
